from typing import Iterator, Optional, List, Tuple
from logging import info, warning
import time
from dataclasses import dataclass
import requests
import re
from bs4 import BeautifulSoup
from peewee import CharField, Model
import telegram
from telegram import Update
from telegram.ext import Updater, MessageHandler, Filters, CallbackContext, CommandHandler
import html
import html5lib

from .globals import tg_channel_id, db, rhash_jwma, updater

JWMA_URL = "http://jwmason.org/the-slack-wire/"
JWMA_PAGED_URL = "http://jwmason.org/the-slack-wire/page/{}/"

HEADERS = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'}


# Database
class PostDB(Model):
    id = CharField(primary_key=True)

    class Meta:
        database = db


# Dataclasses
@dataclass
class Post:
    id: str  # link
    header: str
    short: str
    link: str
    date: str
    tags: List[Tuple[str, str]]
    author: str
    author_url: str
    db_object: PostDB


def parse_jwma(articles) -> Iterator[Post]:
    for article in articles:
        article_id = article['id']
        if PostDB.get_or_none(PostDB.id == article_id):
            continue
        header = article.header.h2.a.text.strip()
        info("Parsing project: %s", header)
        link = article.header.h2.a['href']
        content = article.find("div", {"class": "entry-content"}).p
        if content is not None:
            short = content.text.strip()
        else:
            short = ""
        date_tmp = article.find("time")
        if date_tmp is not None:
            date = date_tmp['datetime']
        tags_tmp = article.find("span", {"class": "tags-links"})
        tags: List[Tuple[str, str]] = []
        if tags_tmp is not None:
            for tag in tags_tmp.find_all("a"):
                tags.append((tag.text.strip(), tag['href']))
        db_obj = PostDB(id=article_id)
        # sub page
        sub_page = requests.get(link, headers=HEADERS)
        sub_soup = BeautifulSoup(sub_page.content, 'html5lib')
        # print("-" * 10)
        # print(sub_soup)
        sub_footer = sub_soup.find("article").footer
        if sub_footer is not None:
            author_tmp = sub_footer.find("span", {"class": "author"}).a

            author = author_tmp.text.strip()
            author_url = author_tmp['href']
        # return
        yield Post(
            id=article_id, header=header, short=short, link=link, date=date,
            tags=tags, author=author, author_url=author_url,
            db_object=db_obj
        )


# Parsers
def parse_jwma_posts(page_num=1) -> Iterator[Post]:
    """Parses the jwma posts and returns all new posts."""
    if page_num < 1:
        warning("Wrong page number.")
        return []
    if page_num == 1:
        page = requests.get(JWMA_URL, headers=HEADERS)
    else:
        page = requests.get(JWMA_PAGED_URL.format(page_num), headers=HEADERS)
    soup = BeautifulSoup(page.content, 'html5lib')
    articles = soup.find_all("article")
    return parse_jwma(articles)


def wait_send(bot, article, **kwargs):
    try:
        message = bot.send_message(**kwargs, timeout=60)
    except telegram.error.RetryAfter as e:
        info(f"Sleeping {e.retry_after} seconds.")
        time.sleep(e.retry_after)
        return wait_send(bot, article, **kwargs)
    except telegram.error.TimedOut:
        info(f"Timed out, sleeping 5 seconds.")
        time.sleep(5)
        return wait_send(bot, article, **kwargs)
    article.db_object.save(force_insert=True)
    return message


def send_posts(posts, bot):
    for article in reversed(list(posts)):
        preview_link = f"http://t.me/iv?url={article.link}&rhash={rhash_jwma}"
        text = f'<b>{article.header}</b><a href="{preview_link}"> </a>'
        text += f'\nBy <a href="{article.author_url}">{article.author}</a>'
        if len(article.short) > 800:
            short = html.escape(article.short[:800] + "...")
        else:
            short = html.escape(article.short)
        text += f"\n\n{short}"
        text += f'\n<a href="{article.link}">Continue Reading</a>'
        if len(article.tags) > 0:
            text += "\n\nTags: "
            tags = []
            for tag, url in article.tags:
                tags.append(f'<a href="{url}">{tag}</a>')
            text += ", ".join(tags)
        info(f"Sending {article.header}.")
        message = wait_send(bot, article, text=text, chat_id=tg_channel_id,
                            parse_mode="HTML")


def update_jwma_posts(callback_context: CallbackContext):
    info("Updating jwmason.")
    info("Parsing posts.")
    jwma_posts = parse_jwma_posts()
    info("Sending posts.")
    send_posts(jwma_posts, callback_context.bot)


def send_backlog():
    info("Loading older posts.")
    for page_num in range(18, 1, -1):
        info(f"Loading page {page_num}")
        jwma_posts = parse_jwma_posts(page_num)
        send_posts(jwma_posts, updater.bot)
